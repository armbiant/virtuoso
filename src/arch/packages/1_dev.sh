#!/usr/bin/env sh

paru -S --noconfirm --disable-download-timeout \
  llvm \
  clang \
  lldb \
  gcc \
  gdb \
  cmake \
  ninja \
  gtest \
  rustup \
  cargo \
  rust-analyzer \
  evcxr_repl \
  bacon \
  webkit2gtk \
  base-devel \
  curl \
  wget \
  openssl \
  appmenu-gtk-module \
  gtk3 \
  libappindicator-gtk3 \
  librsvg \
  libvips \
  go \
  ghc-libs \
  ghc \
  jre17-openjdk \
  jdk-openjdk \
  dotnet-install \
  dotnet-runtime \
  dotnet-sdk \
  dotnet-targeting-pack \
  aspnet-runtime \
  aspnet-targeting-pack \
  mono \
  mono-tools \
  mono-msbuild \
  mono-msbuild-sdkresolver \
  nuget \
  python \
  python-pip \
  python-virtualenv \
  python-pipenv \
  ipython \
  tk \
  pyside6 \
  nodejs-lts-gallium \
  nvm \
  deno \
  npm \
  yarn \
  luajit \
  lua51 \
  luarocks \
  gvim \
  neovim \
  helix \
  tree-sitter \
  vscodium \
  vscodium-marketplace \
  dbeaver \
  sqlitebrowser \
  nodejs-neovim \
  python-pynvim \
  vim-language-server \
  lua-language-server \
  stylua \
  luacheck \
  ldoc \
  lua51-std-_debug \
  eslint \
  prettier \
  typescript \
  typescript-language-server \
  vscode-json-languageserver \
  vscode-html-languageserver \
  tidy \
  vscode-css-languageserver \
  graphql-lsp \
  nodejs-svelte-language-server \
  ungoogled-chromium \
  pyright \
  python-black \
  python-pylint \
  python-debugpy \
  omnisharp-roslyn \
  bash-language-server \
  shellcheck \
  shfmt \
  yaml-language-server \
  aws-cli-v2 \
  aws-sam-cli-bin \
  azure-cli \
  vale-bin \
  codespell \
  graphviz \
  plantuml \
  pandoc \
  doxygen \
  python-sphinx \
  ruby \
  postgresql \
  ;

rustup toolchain install stable
nvm install stable
cat <<END >/etc/sysctl.d/debug.conf
kernel.yama.ptrace_scope=0
END

printf "[VIRTUOSO]: Enable 'chrome://flags/#allow-insecure-localhost' in brave and chromium\n"

printf "[VIRTUOSO]: Installed dev tools\n"
