#!/usr/bin/env sh

yes | paru -S --disable-download-timeout \
  docker \
  docker-compose \
  lazydocker \
  libvirt \
  libguestfs \
  virt-viewer \
  virt-install \
  qemu-desktop \
  cockpit \
  cockpit-machines \
  packagekit \
  iptables-nft \
  dnsmasq \
  dmidecode \
  bridge-utils \
  openbsd-netcat \
  ;
sudo systemctl enable --now libvirtd.service
sudo systemctl enable --now cockpit.socket
sudo systemctl enable --now docker.service
sudo virsh net-autostart default
cat <<END >/etc/sysctl.d/elasticsearch.conf
vm.max_map_count=262144
END
printf "[VIRTUOSO]: Installed virtualization services\n"

sudo usermod -aG libvirt "$(id -un)"
sudo usermod -aG docker "$(id -un)"
printf "[VIRTUOSO]: Added %s user to virtualization groups\n" "$(id -un)"
