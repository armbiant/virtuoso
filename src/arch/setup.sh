#!/usr/bin/env sh

printf "\n[VIRTUOSO]: Creating Arch Linux VM...\n"

ARCH_SCRIPTS="$(realpath "$(dirname "$0")")"
SCRIPTS="$(dirname "$ARCH_SCRIPTS")"
ROOT="$(dirname "$SCRIPTS")"

IMAGE="$1"
if [ ! -f "$IMAGE" ]; then
  printf "[VIRTUOSO]: Please enter an image \n"
  exit 1
fi
printf "[VIRTUOSO]: Using %s image\n" "$IMAGE"

NAME="$(basename -s .iso "$IMAGE")"
printf "[VIRTUOSO]: Using %s name\n" "$NAME"

DISK="$IMAGE.qcow2"
printf "[VIRTUOSO]: Using %s disk\n" "$DISK"

KERNEL="/arch/boot/x86_64/vmlinuz-linux"
INITRD="/arch/boot/x86_64/initramfs-linux.img"

virt-install \
  --connect qemu:///system \
  --name "$NAME" \
  --location "$IMAGE",kernel="$KERNEL",initrd="$INITRD" \
  --osinfo archlinux \
  --disk path="$DISK",size=20 \
  --filesystem source="$ROOT",target=virtuoso \
  --boot uefi \
  --ram 2048 \
  --vcpus 2 \
  --graphics spice \
  --console pty,target_type=serial \
  --extra-args "console=ttyS0" \
  --install kernel_args="console=ttyS0 archisodevice=/dev/sr0"

printf "\n[VIRTUOSO]: Created Arch Linux VM\n"
