#!/usr/bin/env sh

printf "\n[VIRTUOSO]: Installing Arch Linux in WSL...\n"

if [ ! "$(id -un)" = "root" ]; then
  printf "[VIRTUOSO]: Please run this as root\n"
  exit 1
fi

if [ ! "$VIRTUOSO_NOEXEC" ]; then
  if [ ! -d "/var/log/virtuoso" ]; then
    mkdir "/var/log/virtuoso"
  fi
  VIRTUOSO_NOEXEC=1 exec "$0" "$@" 2>&1 | tee "/var/log/virtuoso/wsl"
  exit
fi

USER=$1
if ! echo "$USER" | grep -Eq "[A-Za-z0-9]+"; then
  printf "[VIRTUOSO]: Please enter a valid user name for the 1st argument\n"
  exit 1
fi
printf "[VIRTUOSO]: Using %s user\n" "$USER"

DOTFILES=$2
if ! echo "$DOTFILES" | grep -Eq "https.*"; then
  printf "[VIRTUOSO]: Please enter a valid dotfile repository URL for the 2nd argument\n"
  exit 1
fi
printf "[VIRTUOSO]: Using %s dotfiles\n" "$DOTFILES"

"$ARCH_SCRIPTS/lib/dns.sh" || exit 1
"$ARCH_SCRIPTS/lib/user.sh" "$USER" "$DOTFILES" || exit 1
"$ARCH_SCRIPTS/lib/repos.sh" || exit 1

cat <<END
[VIRTUOSO]: Installed Arch Linux in WSL

Things to do after install:
 - check the logs
 - systemctl reboot
 - run the reboot.sh script
END
