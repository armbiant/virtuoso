#!/usr/bin/env sh

printf "\n[VIRTUOSO]: Installing packages...\n"

ARCH_SCRIPTS="$(realpath "$(dirname "$0")")"

paru -S --noconfirm --disable-download-timeout \
  pacman-cleanup-hook \
  ancient-packages \
  ;

"$ARCH_SCRIPTS/lib/secure-boot.sh"

if [ -d "$ARCH_SCRIPTS/packages" ]; then
  if [ "$WSL_DISTRO_NAME" ]; then
    "$ARCH_SCRIPTS/packages/0_terminal.sh"
    "$ARCH_SCRIPTS/packages/1_dev.sh"
  else
    for script in "$ARCH_SCRIPTS/packages"/?*.sh; do
      if [ -x "$script" ]; then
        if ! "$script"; then
          printf "[VIRTUOSO]: Failed running %s\n" "$script"
        fi
      fi
    done
  fi
fi

printf "\n[VIRTUOSO]: Installed packages\n"
