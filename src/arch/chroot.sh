#!/usr/bin/env sh

ARCH_SCRIPTS="$(realpath "$(dirname "$0")")"

if [ ! "$(id -un)" = "root" ]; then
  printf "[VIRTUOSO]: Please run this in chroot as root\n"
  exit 1
fi

if [ ! "$VIRTUOSO_NOEXEC" ]; then
  if [ ! -d "/var/log/virtuoso" ]; then
    mkdir "/var/log/virtuoso"
  fi
  VIRTUOSO_NOEXEC=1 exec "$0" "$@" 2>&1 | tee "/var/log/virtuoso/chroot"
  exit
fi

ZONEINFO=$1
if ! echo "$ZONEINFO" | grep -Eq "[A-Z][a-z]+\/[A-Z][a-z]+"; then
  printf "[VIRTUOSO]: Please enter a valid zoneinfo for the 1st argument \n"
  exit 1
fi
printf "[VIRTUOSO]: Using %s zoneinfo\n" "$ZONEINFO"

MACHINE=$2
if ! echo "$MACHINE" | grep -Eq "[A-Z]+"; then
  printf "[VIRTUOSO]: Please enter a valid machine name for the 2nd argument\n"
  exit 1
fi
printf "[VIRTUOSO]: Using %s machine\n" "$MACHINE"

USER=$3
if ! echo "$USER" | grep -Eq "[A-Za-z0-9]+"; then
  printf "[VIRTUOSO]: Please enter a valid user name for the 3rd argument\n"
  exit 1
fi
printf "[VIRTUOSO]: Using %s user\n" "$USER"

DOTFILES=$4
if ! echo "$DOTFILES" | grep -Eq "https.*"; then
  printf "[VIRTUOSO]: Please enter a valid dotfile repository URL for the 4th argument\n"
  exit 1
fi
printf "[VIRTUOSO]: Using %s dotfiles\n" "$DOTFILES"

"$ARCH_SCRIPTS/lib/dns.sh" || exit 1
"$ARCH_SCRIPTS/lib/locale.sh" "$ZONEINFO" || exit 1
"$ARCH_SCRIPTS/lib/repos.sh" || exit 1
"$ARCH_SCRIPTS/lib/user.sh" "$USER" "$DOTFILES" || exit 1
"$ARCH_SCRIPTS/lib/bootloader.sh" "$MACHINE" || exit 1
"$ARCH_SCRIPTS/lib/base.sh" "$USER" || exit 1
"$ARCH_SCRIPTS/lib/theme.sh"
