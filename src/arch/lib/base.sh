#!/usr/bin/env sh

printf "\n[VIRTUOSO]: Setting up base...\n"

USER=$1
if ! echo "$USER" | grep -Eq "[A-Za-z0-9]+"; then
  printf "[VIRTUOSO]: Please enter a valid user name for the 1st argument\n"
  exit 1
fi
printf "[VIRTUOSO]: Using %s user\n" "$USER"

if [ ! "$(which paru)" ]; then
  printf "[VIRTUOSO]: Please install paru first\n"
  exit 1
fi

# TODO: https://www.katzien.de/en/posts/2022-02-06-sudo-with-fingerprint-support/
# TODO: laptop detection, libinput-gestures
if ! paru -S --noconfirm --noprogressbar --disable-download-timeout \
  arch-install-scripts \
  reflector \
  namcap \
  snap-pac \
  snap-pac-grub \
  snapper \
  btrfsmaintenance \
  ananicy \
  earlyoom \
  opendoas \
  git \
  openssh \
  gvim \
  man-db \
  man-pages \
  texinfo \
  zsh \
  fish \
  nushell \
  git-delta \
  ranger \
  networkmanager \
  openvpn \
  gufw \
  ppp \
  wireless_tools \
  iw \
  wpa_supplicant \
  network-manager-applet \
  network-manager-sstp \
  networkmanager-fortisslvpn \
  networkmanager-openvpn \
  pipewire \
  pipewire-alsa \
  pipewire-jack \
  pipewire-pulse \
  alsa-utils \
  alsa-plugins \
  realtime-privileges \
  sof-firmware \
  gst-plugin-pipewire \
  pipewire-v4l2 \
  pipewire-zeroconf \
  wireplumber \
  pasystray \
  playerctl \
  pavucontrol \
  bluez-libs \
  libinput \
  libinput-gestures \
  brightnessctl \
  acpi \
  fprintd \
  xorg-server \
  xorg-xauth \
  xorg-xwininfo \
  xorg-xvinfo \
  xorg-xrandr \
  xorg-xkill \
  xorg-xinput \
  xorg-xgamma \
  xorg-xdpyinfo \
  xorg-setxkbmap \
  xorg-util-macros \
  numlockx \
  xclip \
  gvfs \
  gvfs-nfs \
  gvfs-mtp \
  gvfs-gphoto2 \
  lxsession-gtk3 \
  picom \
  libglvnd \
  xorg-mkfontscale \
  xorg-font-util \
  xorg-fonts-100dpi \
  gnu-free-fonts \
  opendesktop-fonts \
  noto-fonts \
  noto-fonts-emoji-flag-git \
  noto-color-emoji-fontconfig \
  ttf-twemoji \
  noto-fonts-extra \
  noto-fonts-cjk \
  nerd-fonts-jetbrains-mono \
  ttf-dejavu \
  ttf-liberation \
  wqy-zenhei \
  sddm \
  betterlockscreen \
  python-dbus-next \
  python-iwlib \
  python-keyring \
  python-psutil \
  python-setproctitle \
  python-pyxdg \
  khal \
  qtile \
  mypy \
  dunst \
  rofi \
  flameshot \
  redshift \
  gnome-keyring \
  kitty \
  starship \
  xarchiver \
  pcmanfm-gtk3 \
  lxtask-gtk3 \
  lxinput-gtk3 \
  lxrandr-gtk3 \
  pamac-aur \
  plasma-framework \
  gtk-engine-murrine \
  kvantum \
  lxappearance-gtk3 \
  beautyline \
  kvantum-theme-sweet-git \
  sweet-gtk-theme-nova-git; then
  printf "\n[VIRTUOSO] Failed installing base\n"
  exit 1
fi
sudo ln -sf \
  /usr/share/fontconfig/conf.avail/75-twemoji.conf \
  /etc/fonts/conf.d/75-twemoji.conf

systemctl enable reflector.timer
systemctl enable btrfs-balance.timer
systemctl enable btrfs-defrag.timer
systemctl enable btrfs-scrub.timer
systemctl enable btrfs-trim.timer
systemctl enable fstrim.timer
systemctl enable snapper-boot.timer
systemctl enable snapper-cleanup.timer
systemctl enable snapper-timeline.timer
systemctl enable ananicy.service
systemctl enable earlyoom.service
systemctl disable sddm.service
systemctl enable sddm-plymouth.service
systemctl enable NetworkManager
systemctl enable ufw
ufw enable

usermod -aG realtime "$USER"

printf "permit persist :wheel\n" | sudo tee /etc/doas.conf
chmod 0400 /etc/doas.conf

mkdir --parents /etc/sudoers.d
printf "%%wheel ALL=(ALL) ALL\n" | sudo tee /etc/sudoers.d/wheel
chmod 0400 /etc/sudoers.d/wheel

mkdir --parents /etc/pacman.d/hooks
mkdir /.bootbackup
cat <<END >/etc/pacman.d/hooks/boot-backup.hook
[Trigger]
Operation = Upgrade
Operation = Install
Operation = Remove
Type = Path
Target = /boot/*

[Action]
Depends = rsync
Description = Backing up boot...
When = PreTransaction
Exec = /usr/bin/rsync -a --delete /boot /.bootbackup
END

mkdir --parents /etc/systemd/journald.conf.d
cat <<END >/etc/systemd/journald.conf.d/max-use.conf
[Journal]
SystemMaxUse=1G
END

mkdir --parents /etc/xorg.conf.d
cat <<END >/etc/xorg.conf.d/30-touchpad.conf
Section "InputClass"
    Identifier "touchpad"
    Driver "libinput"
    MatchIsTouchpad "on"
    Option "Tapping" "on"
    Option "TappingButtonMap" "lmr"
EndSection
END

mkdir --parents /etc/snapper/configs
cat <<END >/etc/snapper/configs/root
SUBVOLUME="/"
FSTYPE="btrfs"
QGROUP=""
END
cat <<END >"/etc/snapper/configs/$USER"
SUBVOLUME="/home/$USER"
FSTYPE="btrfs"
QGROUP=""
END

mkdir --parents /etc/conf.d
cat <<END >/etc/conf.d/snapper
SNAPPER_CONFIGS="root hrvoje"
END

paru -Qqe

printf "\n[VIRTUOSO]: Base was set up\n"
