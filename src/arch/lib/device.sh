#!/usr/bin/env sh

printf "\n[VIRTUOSO]: Setting up device...\n"

USER=$1
if ! echo "$USER" | grep -Eq "[A-Za-z0-9]+"; then
  printf "[VIRTUOSO]: Please enter a valid user name for the 1st argument"
  exit 1
fi
printf "[VIRTUOSO]: Using %s user\n" "$USER"

DEVICE=$2
if [ ! -b "$DEVICE" ]; then
  printf "[VIRTUOSO]: Please enter a valid block device for the 2nd argument"
  exit 1
fi
printf "[VIRTUOSO]: Using %s device\n" "$DEVICE"

START=$3
if echo "$START" | grep -Eq "[0-9]+"; then
  BOOT_END="$((START + 5))"GB
  START="$START"GB
else
  BOOT_END=5GB
  START=0%
fi
printf "[VIRTUOSO]: Using %s start\n" "$START"

END=$4
if echo "$END" | grep -Eq "[0-9]+"; then
  END="$END"GB
else
  END=100%
fi
printf "[VIRTUOSO]: Using %s end\n" "$END"

BTRFS="rw,noatime,compress=zstd:3,ssd,space_cache=v2,autodefrag,discard=async"
printf "[VIRTUOSO]: Using %s btrfs options\n" "$BTRFS"

# NOTE: prompts user if they want to wipe the device first
parted "$DEVICE" mktable gpt
parted --script "$DEVICE" mkpart arch-boot fat32 "$START" "$BOOT_END"
# NOTE: make this shit better
BOOT="$(parted /dev/nvme0n1 print | grep boot | awk '{ print $1; }')"
ROOT=$((BOOT + 1))
parted --script "$DEVICE" toggle "$BOOT" esp
parted --script "$DEVICE" mkpart root btrfs "$BOOT_END" "$END"
part() {
  if echo "$1" | grep -q "nvme"; then
    echo "$1p$2"
  else
    echo "$1$2"
  fi
}
subvol() {
  if [ "$2" ]; then
    mkdir --parents "/mnt$2"
  fi

  btrfs subvolume create "/mnt$1"
  mount -o "$BTRFS,subvol=$1" "$(part "$DEVICE" "$ROOT")" "/mnt$1"
}
mkfs.fat -F 32 "$(part "$DEVICE" "$BOOT")"
mkfs.btrfs -f "$(part "$DEVICE" "$ROOT")"

printf "[VIRTUOSO]: Partitioned %s device\n" "$DEVICE"
parted "$DEVICE" print

mount -o "$BTRFS" "$(part "$DEVICE" "$ROOT")" /mnt
mkdir /mnt/boot
mount "$(part "$DEVICE" "$BOOT")" /mnt/boot
subvol "/.swap"
btrfs filesystem mkswapfile "/.swap/swapfile"
swapon "/mnt/.swap/swapfile"
subvol "/.snapshots"
subvol "/var/log" "/var"
subvol "/home/$USER" "/home"
subvol "/home/$USER/.snapshots"

printf "[VIRTUOSO]: Mounted %s device\n" "$DEVICE"
btrfs subvolume list /mnt
mount | grep "$DEVICE"

printf "\n[VIRTUOSO]: The device was set up\n"
