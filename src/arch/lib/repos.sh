#!/usr/bin/env sh

printf "\n[VIRTUOSO]: Configuring repositories...\n"

#shellcheck disable=SC2046
if ! pacman -S --noconfirm --noprogressbar $(
  if [ "$WSL_DISTRO_NAME" ]; then
    echo --ignore fakeroot base-devel
  else
    echo base-devel
  fi
); then
  printf "[VIRTUOSO]: Failed installing base-devel...\n"
  exit 1
fi

pacman-key --recv-key FBA220DFC880C036 --keyserver keyserver.ubuntu.com
pacman-key --lsign-key FBA220DFC880C036
if ! pacman -U --noconfirm --noprogressbar --disable-download-timeout \
  'https://cdn-mirror.chaotic.cx/chaotic-aur/chaotic-keyring.pkg.tar.zst' \
  'https://cdn-mirror.chaotic.cx/chaotic-aur/chaotic-mirrorlist.pkg.tar.zst'; then
  printf "[VIRTUOSO]: Failed installing chaotic keyring...\n"
  exit 1
fi

if ! curl -sL https://blackarch.org/strap.sh | sudo bash; then
  printf "[VIRTUOSO]: Failed installing blackarch keyring...\n"
  exit 1
fi

cat <<END >/etc/pacman.conf
[options]
HoldPkg = pacman glibc
Architecture = auto
Color
CheckSpace
ParallelDownloads = 5
ILoveCandy
SigLevel = Required DatabaseOptional
LocalFileSigLevel = Optional

[core]
Include = /etc/pacman.d/mirrorlist

[extra]
Include = /etc/pacman.d/mirrorlist

[community]
Include = /etc/pacman.d/mirrorlist

[multilib]
Include = /etc/pacman.d/mirrorlist

[chaotic-aur]
Include = /etc/pacman.d/chaotic-mirrorlist

[blackarch]
Include = /etc/pacman.d/blackarch-mirrorlist
END

if [ "$WSL_DISTRO_NAME" ]; then
  printf "\nIgnorePkg = fakeroot" >>/etc/pacman.conf
else
  printf "\nIgnorePkg = ttf-google-fonts-git" >>/etc/pacman.conf
fi

if ! pacman -Syyu --noconfirm --noprogressbar --disable-download-timeout; then
  printf "[VIRTUOSO]: Failed updating repositories...\n"
  exit 1
fi

if ! pacman -S --noconfirm --noprogressbar --disable-download-timeout paru; then
  printf "[VIRTUOSO]: Failed instaling paru...\n"
  exit 1
fi

if ! paru -Syyu --noconfirm --noprogressbar --disable-download-timeout; then
  printf "[VIRTUOSO]: Failed updating repositories with paru...\n"
  exit 1
fi

cat /etc/pacman.conf

printf "\n[VIRTUOSO]: Configured repositories\n"
