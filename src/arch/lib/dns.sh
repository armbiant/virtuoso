#!/usr/bin/env sh

printf "\n[VIRTUOSO]: Configuring DNS...\n"

cat <<END >/etc/resolv.conf
nameserver 1.1.1.1
nameserver 1.0.0.1
END

if [ "$WSL_DISTRO_NAME" ]; then
  cat <<END >/etc/wsl.conf
[user]
default=$USER

[network]
generateResolvConf = false
END
  chattr +i /etc/resolv.conf
fi

cat /etc/resolv.conf
if [ "$WSL_DISTRO_NAME" ]; then
  cat /etc/wsl.conf
fi

printf "\n[VIRTUOSO]: DNS configured\n"
