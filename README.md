# Virtuoso

Virtuoso is a collection of scripts for setting up virtual machines and
installing operating systems.

TOOD: use pyinfra

## Download using `CURL`

```shell
cd /opt;
LINK="https://gitlab.com/Hrle/virtuoso/-/archive/main/virtuoso-main.tar.gz";
curl -L "$LINK" | tar -xz;
mv virtuoso-main virtuoso;
cd virtuoso;
```

## Mount inside `VM`

```shell
mkdir /opt/virtuoso;
sudo mount -t 9p -o trans=virtio virtuoso /opt/virtuoso
cd /opt/virtuoso;
```
